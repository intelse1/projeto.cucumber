#Author: edu.intelse@gmail.com

Feature: Login 
Como usuario  do site saucedemo
Quero informar dados de acesso
Para realizar um login

Background: Acessar tela login 
Given que esteja na pagina de login

Scenario: Login com sucesso
 
When enviar os dados de login  
Then login realizado com sucesso


Scenario: Login invalido

* nao preencher o usuario
* preencher a senha 
* clicar no botao login 
* login nao sera realizado com mensagem de usuario obrigatório

Scenario: Login invalido
When preencher o usuario
But nao preencher a senha
And clicar no botao login
Then login nao sera realizado com mensagem de senha obrigatoria