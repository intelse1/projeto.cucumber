#Author: edu.intelse@gmail.com

	Feature: Login 
	Como usuario  do site saucedemo
	Quero informar dados de acesso
	Para realizar um login

	Background: Acessar tela login 
		Given que esteja na pagina de login

  Scenario Outline: Cenário de Validação de Login
   	When informar o <usuario>
   	And informar a <senha>
   	Then validar mensagem <mensagem> 
   

    Examples: 
      | usuario  				|senha 					| mensagem  |
   		| "standard_user" |"secret_sauce" | "https://www.saucedemo.com/inventory.html"|
      | "standard_user" |"" 						| "Epic sadface: Password is required"|
      | "" 							|"secret_sauce"	| "Epic sadface: Username is required"|
