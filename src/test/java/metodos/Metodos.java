package metodos;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;

import drivers.Drivers;

public class Metodos extends Drivers{

	public void escrever(By elemento, String texto, String passo) {
		try {
			driver.findElement(elemento).sendKeys(texto);
		} catch (Exception e) {
			System.out.println("********** ERRO ao ESCREVER PASSO ************" + passo);
			System.out.println("********** CAUSA DO ERRO ************" + e.getCause());
			System.out.println("********** MENSAGEM DO ERRO ************" + e.getMessage());			
		}
	}
	
	public void clicar(By elemento, String passo) {
		try {
			driver.findElement(elemento).click();
		} catch (Exception e) {
			System.out.println("********** ERRO AO CLICAR PASSO ************" + passo);
			System.out.println("********** CAUSA DO ERRO ************" + e.getCause());
			System.out.println("********** MENSAGEM DO ERRO ************" + e.getMessage());	
		}
	}
	
	public void validarUrl(String urlDesejada) {
		try {
			assertEquals(urlDesejada, driver.getCurrentUrl());
		} catch (Exception e) {
			System.out.println("********** URL DIFERENTE DA DESEJADA ************");
			System.out.println("********** CAUSA DO ERRO ************" + e.getCause());
			System.out.println("********** MENSAGEM DO ERRO ************" + e.getMessage());
			
		}
	}
	
	public void validarTexto(By elemento, String msgEsperada) {
		try {
			assertEquals(msgEsperada, driver.findElement(elemento).getText());
		} catch (Exception e) {
			System.out.println("********** ERRO AO VALIDAR TEXTO ************");
			System.out.println("********** CAUSA DO ERRO ************" + e.getCause());
			System.out.println("********** MENSAGEM DO ERRO ************" + e.getMessage());
	
		}
	}
	
}
