package page;
import org.openqa.selenium.By;

import metodos.Metodos;

public class LoginPage {
	
	Metodos metodos = new Metodos();
	By usuario = By.id("user-name");
	By senha = By.id("password");
	By btnLogin = By.id("login-button");
	By msgError = By.xpath("//h3[@data-test='error']");
	String urlLogado = "https://www.saucedemo.com/inventory.html";
	
	
	public void login(String usuario, String senha) {
		metodos.escrever(this.usuario, usuario, "Informar Usuario para login");
		metodos.escrever(this.senha, senha, "informar Senha para login");
		metodos.clicar(btnLogin, "Clicar no botao login");
		
	}
	
	public void validarLogin() {
		metodos.validarUrl(urlLogado);
	}
	
	public void preencherUsuario(String usuario) {
		metodos.escrever(this.usuario, usuario, "informar os usuarios para login");
	}
	
	public void preencherSenha(String senha) {
		metodos.escrever(this.senha, senha, "informar a senha para login");
	}
	
	public void clicarBtnLogin() {
		metodos.clicar(this.btnLogin, "Clciar no botao para login");
	}

	public void validarMsgErro(String msgEsperada) {

		if(msgEsperada.equals(urlLogado)) {
			metodos.validarUrl(msgEsperada);
		}else 
			metodos.validarTexto(msgError, msgEsperada);
	
		
	}
}
