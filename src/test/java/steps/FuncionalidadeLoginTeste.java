package steps;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import page.LoginPage;
import runner.Executa;

public class FuncionalidadeLoginTeste {

	LoginPage login = new LoginPage();
	
	String msgUsuarioObrigatorio = "Epic sadface: Username is required";
	String msgSenhaObrigatoria = "Epic sadface: Password is required";
	
	@After
	public void testefinalizado(){
		//Executa.exitBrowser();
	}
	
	@Given("que esteja na pagina de login")
	public void que_esteja_na_pagina_de_login() {
	   Executa.openBrowser();
	}

	@When("enviar os dados de login")
	public void enviar_os_dados_de_login() {
		login.login("standard_user", "secret_sauce");
	}

	@Then("login realizado com sucesso")
	public void login_realizado_com_sucesso() {
		login.validarLogin();
	}
	

	@Given("nao preencher o usuario")
	public void nao_preencher_o_usuario() {
	  login.preencherUsuario("");
	}

	@Given("preencher a senha")
	public void preencher_a_senha() {
	    login.preencherSenha("secret_sauce");
	}

	@Given("clicar no botao login")
	public void clicar_no_botao_login() {
	    login.clicarBtnLogin();
	}

	@Given("login nao sera realizado com mensagem de usuario obrigatório")
	public void login_nao_sera_realizado_com_mensagem_de_usuario_obrigatório() {
		login.validarMsgErro(msgUsuarioObrigatorio);
	}
	
	@When("preencher o usuario")
	public void preencher_o_usuario() {
	    login.preencherUsuario("standard_user");
	}

	@When("nao preencher a senha")
	public void nao_preencher_a_senha() {
	    	login.preencherSenha("");
	}

	@Then("login nao sera realizado com mensagem de senha obrigatoria")
	public void login_nao_sera_realizado_com_mensagem_de_senha_obrigatoria() {
	   login.validarMsgErro(msgSenhaObrigatoria);
	}
	
	
	
	@When("informar o {string}")
	public void informar_o(String usuario) {
		login.preencherUsuario(usuario);
	}

	@When("informar a {string}")
	public void informar_a(String senha) {
	    login.preencherSenha(senha);
	
	}

	@Then("validar mensagem {string}")
	public void validar_mensagem(String mensagem) {
		login.clicarBtnLogin();
	    login.validarMsgErro(mensagem);
	}
	
}
