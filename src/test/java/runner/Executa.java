package runner;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import drivers.Drivers;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Executa extends Drivers{
	
	public static void openBrowser() {
		String browser = "Chrome";
		String ambiente = "https://saucedemo.com";
		
		if(browser.equalsIgnoreCase("Chrome")) {
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
		}else if (browser.equalsIgnoreCase("Firefox")) {
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
		}else {
			System.out.println("Opção inválida!");
		}
		driver.manage().window().maximize();
		driver.get(ambiente);
	}
	
	public static void exitBrowser() {
		driver.quit();
	}

}
